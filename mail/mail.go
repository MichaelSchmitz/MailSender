package mail

import (
	"crypto/tls"
	"log"
	"net"
	"net/smtp"
	"os"

	"gitlab.com/MichaelSchmitz/MailSender/models"
)

func SendSMTP(body models.MailBody) bool {
	from := os.Getenv("SMTP_FROM")
	password := os.Getenv("SMTP_PW")

	addr := os.Getenv("SMTP_HOST")
	host, _, _ := net.SplitHostPort(addr)

	msg := []byte("From: " + from + "\r\n" +
		"To: " + from + "\r\n" +
		"Subject: Contact via Form\r\n\r\n" +
		GetBody(body) + "\r\n")

	auth := smtp.PlainAuth("", from, password, host)
	tlsconfig := &tls.Config{
		ServerName: host,
	}
	conn, err := tls.Dial("tcp", addr, tlsconfig)
	if err != nil {
		log.Panic(err)
		return false
	}
	client, err := smtp.NewClient(conn, host)
	if err != nil {
		log.Panic(err)
		return false
	}
	if err = client.Auth(auth); err != nil {
		log.Panic(err)
		return false
	}
	if err = client.Mail(from); err != nil {
		log.Panic(err)
		return false
	}
	if err = client.Rcpt(from); err != nil {
		log.Panic(err)
		return false
	}
	w, err := client.Data()
	if err != nil {
		log.Panic(err)
		return false
	}
	_, err = w.Write(msg)
	if err != nil {
		log.Panic(err)
		return false
	}
	err = w.Close()
	if err != nil {
		log.Panic(err)
		return false
	}
	client.Quit()
	return true
}

func GetBody(body models.MailBody) string {
	return "Message from: " + body.Name + "\r\nContact Mail: " + body.Mail + "\r\nMessage:\r\n" + body.Msg
}
